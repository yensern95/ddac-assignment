﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDACAssignment.Models
{
    public class FlightBookingHistory
    {
        public int fhid { get; set; }
        public string fname { get; set; }
        public DateTime dataBooking { get; set; }
        public DateTime dataFlight { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
        public string fclass { get; set; }
        public double price{ get; set; }
        public string status { get; set; }

    }
}