﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDACAssignment.Models
{
    public class FlightSchedule
    {
        public int fsid { get; set; }
        public string fname { get; set; }
        public DateTime date { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
        public string status { get; set; }

    }
}