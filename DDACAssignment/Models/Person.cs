﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDACAssignment.Models
{
    public class Person
    {
        public static Person p = new Person();
        
        public int pid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string contactNum { get; set; }
        public string email { get; set; }
        public string password { get; set; }

    }
}