﻿using DDACAssignment.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DDACAssignment.Controllers
{
    public class FlightScheduleController : Controller
    {
        // GET: FlightSchedule
        public ActionResult Index()
        {

            List<FlightSchedule> schedules = new List<FlightSchedule>();

            String connString = "Server=tcp:uiaflightddac.database.windows.net,1433;Initial Catalog=UIAFlight;Persist Security Info=False;User ID=web_user;Password=Abc123123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            SqlConnection conn = new SqlConnection(connString);

            conn.Open();


            String query = "SELECT * FROM FlightSchedule";


            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = conn;

            SqlDataReader reader = cmd.ExecuteReader();


            while (reader.Read())
            {
                schedules.Add(new FlightSchedule()
                {
                    fsid = (int)reader["FSID"],
                    fname = reader["Fname"].ToString(),
                    date = Convert.ToDateTime(reader["Date"]),
                    origin = reader["Origin"].ToString(),
                    destination = reader["Destination"].ToString(),
                    status = reader["Status"].ToString(),
                });
            }
            ViewBag.Schedules = schedules;
            return View("../Flight/Schedule");
        }
    }
}