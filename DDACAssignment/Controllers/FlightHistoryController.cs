﻿using DDACAssignment.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DDACAssignment.Controllers
{
    public class FlightHistoryController : Controller
    {
        // GET: FlightHistory
        public ActionResult Index()
        {
            List<FlightBookingHistory> histories = new List<FlightBookingHistory>();

            String connString = "Server=tcp:uiaflightddac.database.windows.net,1433;Initial Catalog=UIAFlight;Persist Security Info=False;User ID=web_user;Password=Abc123123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            SqlConnection conn = new SqlConnection(connString);

            conn.Open();


            String query = "SELECT * FROM FlightBookingHistory WHERE PID = " + Person.p.pid;


            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = conn;

            SqlDataReader reader = cmd.ExecuteReader();
            

            while (reader.Read())
            {
               histories.Add(new FlightBookingHistory() {
                    fhid = (int)reader["FHID"],
                    fname = reader["Fname"].ToString(),
                    dataBooking = Convert.ToDateTime(reader["DateBooking"]),
                    dataFlight = Convert.ToDateTime(reader["DateFlight"]),
                    origin = reader["Origin"].ToString(),
                    destination = reader["Destination"].ToString(),
                    fclass = reader["FClass"].ToString(),
                    price = (double) reader["Price"],
                    status = reader["Status"].ToString(),
                });
            }
            ViewBag.Histories = histories;
            return View("../Flight/History");
        }
    }
}