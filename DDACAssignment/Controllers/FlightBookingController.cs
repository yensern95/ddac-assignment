﻿using DDACAssignment.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DDACAssignment.Controllers
{
    public class FlightBookingController : Controller
    {
        // GET: FlightBooking
        public ActionResult Index()
        {
            return View("../Flight/Booking");
        }
        public ActionResult Flight_Booking(string origin, string destination, string date, string price)
        {

            //book the flight here... get the data from html form
            string fclass = Request.Form["flightClass"];
            string fname = Request.Form["flightname"];

            DateTime oDate = DateTime.Now;
            if (date.Equals(""))
                oDate = DateTime.Now;
            else
                oDate = Convert.ToDateTime(date);

            string oDateString = oDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

            float fprice = 260;
            if (fclass.Equals("Economic Class"))
                fprice = 260;
            else if (fclass.Equals("Business Class"))
                fprice = 390;
            else
                fprice = 520;

            String connString = "Server=tcp:uiaflightddac.database.windows.net,1433;Initial Catalog=UIAFlight;Persist Security Info=False;User ID=web_user;Password=Abc123123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            SqlConnection conn = new SqlConnection(connString);

            conn.Open();
            DateTime myDateTime = DateTime.Now;
            string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

            String query = "INSERT INTO [dbo].[FlightBookingHistory] ([Fname],[DateBooking],[DateFlight],[Origin],[Destination],[FClass],[Price],[Status], [PID]) VALUES ('" + fname + "', '" + sqlFormattedDate + "', '" + oDateString + "', '" + origin + "', '" + destination + "', '" + fclass + "', " + fprice + ", 'WAITING' ,'" + Person.p.pid + "')";
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();

            conn.Close();


            TempData["success"] = "<script>alert('Flight booked succesfully!!!');</script>";



            return View("../Flight/Booking");
        }
    }
}